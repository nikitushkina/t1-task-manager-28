package ru.t1.nikitushkina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-index";

    @NotNull
    public static final String DESCRIPTION = "Remove task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        @Nullable final String userId = getUserId();
        System.out.println("[ENTER INDEX]");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(userId, index);
    }

}
