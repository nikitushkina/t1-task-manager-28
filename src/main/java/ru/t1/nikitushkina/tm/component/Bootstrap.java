package ru.t1.nikitushkina.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.nikitushkina.tm.api.repository.ICommandRepository;
import ru.t1.nikitushkina.tm.api.repository.IProjectRepository;
import ru.t1.nikitushkina.tm.api.repository.ITaskRepository;
import ru.t1.nikitushkina.tm.api.repository.IUserRepository;
import ru.t1.nikitushkina.tm.api.service.*;
import ru.t1.nikitushkina.tm.command.AbstractCommand;
import ru.t1.nikitushkina.tm.command.data.AbstractDataCommand;
import ru.t1.nikitushkina.tm.command.data.DataBase64LoadCommand;
import ru.t1.nikitushkina.tm.command.data.DataBinaryLoadCommand;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.nikitushkina.tm.exception.system.CommandNotSupportedException;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.model.User;
import ru.t1.nikitushkina.tm.repository.CommandRepository;
import ru.t1.nikitushkina.tm.repository.ProjectRepository;
import ru.t1.nikitushkina.tm.repository.TaskRepository;
import ru.t1.nikitushkina.tm.repository.UserRepository;
import ru.t1.nikitushkina.tm.service.*;
import ru.t1.nikitushkina.tm.util.SystemUtil;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.nikitushkina.tm.command";
    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract((clazz.getModifiers()))) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initData() {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        if (checkBinary) processCommand(DataBinaryLoadCommand.NAME, false);
        if (checkBinary) return;
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) processCommand(DataBase64LoadCommand.NAME, false);
    }

    void initDemoData() {
        @NotNull final User user1 = userService.create("user1", "user1pwd", "user1@a.ru");
        @NotNull final User user2 = userService.create("user2", "user2pwd", "user2@a.ru");
        @NotNull final User user3 = userService.create("user3", "user3pwd", Role.ADMIN);

        projectService.add(user1.getId(), new Project("Project 02", "p02", Status.NOT_STARTED));
        projectService.add(user2.getId(), new Project("Project 03", "p03", Status.COMPLETED));
        projectService.add(user3.getId(), new Project("Project 01", "p01", Status.IN_PROGRESS));

        taskService.create(user1.getId(), "Task 03", "t03");
        taskService.create(user2.getId(), "Task 01", "t01");
        taskService.create(user3.getId(), "Task 02", "t02");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
        System.exit(0);
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String argument = args[0];
        processArgument(argument);
    }

    private void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    private void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        initPID();
        if (args.length > 0) processArguments(args);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
