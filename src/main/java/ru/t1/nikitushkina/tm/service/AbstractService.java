package ru.t1.nikitushkina.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.IRepository;
import ru.t1.nikitushkina.tm.api.service.IService;
import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.exception.field.IdEmptyException;
import ru.t1.nikitushkina.tm.exception.field.IndexIncorrectException;
import ru.t1.nikitushkina.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>>
        implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        return repository.set(models);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}
